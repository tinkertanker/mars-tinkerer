﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardInput : MonoBehaviour
{
	private int playerNumber;

    // Start is called before the first frame update
    void Start()
    {   
        // get the player number
        playerNumber = GetComponent<Player>().playerNumber;
    }

    // Update is called once per frame
    void FixedUpdate()
    {   
        // get vertical and horizontal inputs from the input
        float verticalInput = Input.GetAxis("Vertical"+playerNumber);
        float horizontalInput = Input.GetAxis("Horizontal"+playerNumber);

        // send to playermovement
        GetComponent<PlayerMovement>().updown = verticalInput;
        GetComponent<PlayerMovement>().leftright = horizontalInput;
    }
}
