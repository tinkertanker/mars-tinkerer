﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{   
    // stores the time left for the powerup
    private float countdown; 

    // stores the audio clip and the audio source to play when the power up is being collected
    public AudioClip powerUpCollectedMusic; 
    public AudioSource audioSource; 

    // power ups will be available for 10s, after which it will be destroyed
    public float powerUpAvailablity = 10f; 

    void Start()
    {   
        // initiate the countdown to destroy the power up if not collected
        countdown = powerUpAvailablity;

        // set the audio clip to the audio source
        audioSource.clip = powerUpCollectedMusic;
    }

    void Update()
    {
        //rotates 1 degrees per second around y axis
        transform.Rotate(0f, 150f * Time.deltaTime, 0f); 

        // check time to destroy
        if (countdown < 0f)
        {   
            // destory the power up when countdown is up
            Object.Destroy(gameObject, powerUpCollectedMusic.length);
        }
        else
        {
            // count down
            countdown -= Time.deltaTime;
            if (countdown == 0f)
            {
                countdown = -1f;
            }
        }
    }


    void OnTriggerEnter(Collider col)
    {   
        // if the power up spawns on environment layer..
        if ( col.gameObject.layer == 13 ) 
        {
            // destroy it on the next frame
            countdown = -1f; 

        // if the power up collides with a player's charactercontroller..
        } else if ( col.gameObject.layer == 9 && col.GetType() == typeof(UnityEngine.CharacterController) ) 
        {
            // play the audio
            audioSource.Play();

            // set the countdown for the power up to be destroyed next update
            countdown = -1f;

            // if it is a speed power up ..
            if (gameObject.tag == "Speed")
            {   
                // add speed to playermovement
                col.GetComponent<PlayerMovement>().addSpeed();

            // if it is a invulnerability power up ..
            }else if (gameObject.tag == "Invulnerability")
            {
                // make the player invulnerable
                col.GetComponent<PlayerMovement>().becomeInvulnerable();
            }
            
        }
    }   
}
