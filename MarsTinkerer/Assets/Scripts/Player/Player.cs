using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	// store the player number an
	public int playerNumber {get; private set;}

	// store secondary material
	public Material player2Mat;

	public void setup(int playerNumber, string control, string portName)
	{
		this.playerNumber = playerNumber;

		// if player is player 2..
		if (playerNumber == 2)
		{
			// set to black
			this.gameObject.GetComponentInChildren<Renderer>().material = player2Mat;
		}

		// if control is serial
		if ( control != "Keyboard" )
		{
			// turn off keyboard controls
			GetComponent<KeyboardInput>().enabled = false;

			// set port number and turn on serial controls
			GetComponent<SerialController>().portName = portName;
			GetComponent<SerialController>().enabled = true;
		}
	}	
}
