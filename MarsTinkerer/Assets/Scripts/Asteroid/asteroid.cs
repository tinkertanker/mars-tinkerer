using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class asteroid : MonoBehaviour
{	
	// store audiosource and music
	public AudioSource audioSource;
	public AudioClip asteroidHit;

	private void Start()
	{
   		audioSource.clip = asteroidHit;		
	}

   	private void OnTriggerEnter(Collider other)
   	{	
   		// if other object is a player ...
   		if(other.gameObject.layer == 9)
   		{	
   			// and the part of the player we hit is the charactercontroller ..
   			if (other.GetType() == typeof(UnityEngine.CharacterController))
   			{	
   				// stun him
	   			other.gameObject.GetComponent<PlayerMovement>().stun();

	   			// play the stun music
		   		audioSource.Play();

		   		// destroy the asteroid
	   			Destroy(gameObject, asteroidHit.length);
   			}

   		// otherwise, if asteroid just hit other random stuff
   		} else {
   			// just destroy the object
   			Destroy(gameObject);
   		}
   	}

}
