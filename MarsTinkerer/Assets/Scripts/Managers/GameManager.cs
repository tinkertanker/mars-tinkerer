using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{

	// store the pause screen and the set up menu
	public GameObject pauseScreen; // pause panel from the game scene
	// Reference to the Pause UI (panel).
	public Setup m_Setup; // set up from the game scene
	// Reference to the Setup dialog box

	// Reference to the CameraControl script for control during different phases.
	public CameraControl m_CameraControl; // camera rig in game scene

	// tile prefab and parent
	public GameObject TilePrefab; 
	public Transform TileParent;

	// asteroid Prefab and parent
	public GameObject AsteroidPrefab; 
	public Transform AsteroidParent;
	private float asteroidSpawnTime;

	// variables for parts spawning
	public List<GameObject> partsPrefabs;
	public Transform partsParent;
	private int partsToSpawn;
	private float partSpawnTime;
	private List<GameObject> parts;
	[HideInInspector]public int partsSpawned = 0;

	// variables for player spawning
	public GameObject astronautPrefab;
	[HideInInspector]public List<GameObject> players;
	private int playersToSpawn;
	public List<Transform> spawnPositions;

	// Lists to store the controls and ports
	private List<string> controls;
	private List<string> ports;

	// power up prefabs
	public GameObject speedPowerUpPrefab;
	public GameObject invulnerabiliyPowerUpPrefab;
	[HideInInspector] public float powerUpSpawntime;
	private GameObject[] powerUpPrefabs;
	public Transform powerUpParent;
	private List<GameObject> powerUps;

	// audio clips and source
	public AudioClip gameMusic;
	public AudioClip winMusic;
	public AudioClip loseMusic;
	public AudioClip menuMusic;
	public AudioSource audioSource;

	// score storing
	[HideInInspector] public int score = 0;

	// Used to store the time of the round
	[HideInInspector] public string player1Control;
	[HideInInspector] public string player2Control;

	// store player names
	private string player1Name; 
	private string player2Name;

	// Actual remaining game time
	private float remainingTime;

	// store the waits
	private WaitForSeconds shortWait; 
	private WaitForSeconds longWait;

	// bool for when the game is running
	private bool roundRunning; // Set true in RoundStarting and false in RoundEnding

	// bools for game pausing
	private bool isPaused; // this will pause the game
	private bool canBePaused; // this will determine if th game can be paused
	// Game can only be paused while a round is running.

	// bool for last20s protocol
	private bool last20secActivated = false;
	
	// empty camera target
	public Transform[] cameraTargetTemp; // size =0
	// When all tanks are destroyed at the end of the game we set the camera's targets to this transform until more tanks are spawned.



	private void Start ()
	{
		// Create the delays so they only have to be made once.
		shortWait = new WaitForSeconds (2f);
		longWait = new WaitForSeconds (3f);
        
		// Set Array of Powerups
		powerUpPrefabs = new GameObject[] { speedPowerUpPrefab, invulnerabiliyPowerUpPrefab }; // m_Health removed form list

		// Make extra settings inactive
		m_Setup.m_ExtraSettings.SetActive (false);

		// spawn tiles first
		spawnAllTiles();

	}

	private void Update ()
	{
		// if the game can be paused and esc is pressed ..
		if(Input.GetKeyDown(KeyCode.Escape) && canBePaused)
		{	
			// pause the game
			pauseGame();
		}

		// Count down
		if( roundRunning )
		{	
			remainingTime -= Time.deltaTime;
			if ( remainingTime < 20f )
			{
				if ( !last20secActivated )
				{
					activateLast20secsProtocol();					
				}
			}
		}
        
		// If time not up..
		if (remainingTime > 0f && roundRunning) 
		{
			// update time on the counter
			m_Setup.m_CounterText.text = Mathf.Floor (remainingTime / 60) + ":" + Mathf.Round (remainingTime % 60).ToString ("00");
		
			// If time to spawn powerup
			if (remainingTime < powerUpSpawntime && powerUpSpawntime > 0) 
			{
				// spawn the power up
				spawnPowerUp ();

				// set the power up to between 10 to 20s from now
				powerUpSpawntime = remainingTime - Random.Range (10f, 20f);
			}

			// if time to spawn asteroid
			if (remainingTime < asteroidSpawnTime && asteroidSpawnTime > 0)
			{
				// spawn the asteroid
				spawnAsteroid();

				// Asteroid to spawn every 0 to 10s from now
				asteroidSpawnTime = remainingTime - Random.Range(0f, 10f);
			}

			// if parts have yet to finish spawning ..
			if( partsSpawned < partsToSpawn )
			{	
				// is time to spawn a part
				if (remainingTime < partSpawnTime && partSpawnTime > 0)
				{	
					// spawn part
					spawnPart();

					// set time of next spawn to between 0 to 10s from now
					partSpawnTime = remainingTime - Random.Range(0f, 10);				
				}
			}

		// otherwise..
		} else 
		{
			// its blank
			m_Setup.m_CounterText.text = "";
		}
        

	}

	public void OnClickButton (string choice)
	{
		if (choice == "start") 
		{
			// Set team name
			player1Name = (m_Setup.m_BlueTeamNameInput.text == "") ? "Player 1" : m_Setup.m_BlueTeamNameInput.text;

			// Set player control scheme/COM port
			player1Control = m_Setup.m_BlueControl.options [m_Setup.m_BlueControl.value].text;

			// Add controls and ports to lists
			controls = new List<string>();
			ports = new List<string>();
			controls.Add(player1Control);
			ports.Add(GetPortName(m_Setup.m_BluePort.text));

			// Alternative settings for solo/1v1
			if (m_Setup.m_Gamemode.value == 0) {
				playersToSpawn = 1;
			} else {
				// Apply the following settings for other player
				playersToSpawn = 2;
				player2Name = (m_Setup.m_RedTeamNameInput.text == "") ? "Player 2" : m_Setup.m_RedTeamNameInput.text;
				player2Control = m_Setup.m_RedControl.options [m_Setup.m_RedControl.value].text;
				controls.Add(player2Control);
				ports.Add( GetPortName(m_Setup.m_RedPort.text) );
			}

			// Start music
			audioSource.clip = gameMusic;
			audioSource.Play ();

			// set the remaining time
			remainingTime = float.Parse(m_Setup.m_GameLength.text);

			// determine parts to spawn
			partsToSpawn = (int)((remainingTime-20f)/40f); // 40s for each part and additional 20s
			if (partsToSpawn < 1)
			{
				partsToSpawn = 1;
			}

			//Perform initialization
			StartCoroutine (startGame ());

		}
	}

	private void spawnAsteroid()
	{
		// let position be a random vector between the range ... and at height of 100f
		Vector3 position = new Vector3 (Random.Range (-40.0f, 40.0f), 100f, Random.Range (-40.0f, 40.0f));

		// instantiate asteroid at position
		GameObject asteroid = Instantiate (AsteroidPrefab, AsteroidParent);
		asteroid.transform.position = position;
	}

	private void spawnAllTiles()
	{
		// at height -0.49f
		float y = -0.49f;

		// instantiate tile across the entire map
		for( float x = -50f; x <50; x+=4f)
		{
			for( float z = -50f; z <50; z+=4f)
			{
				GameObject tile = Instantiate(TilePrefab, TileParent);
				tile.transform.position =  new Vector3(x, y, z);
			}
		}
	}

	private void spawnPowerUp ()
	{	
		// set position
		Vector3 position = new Vector3 (Random.Range (-40.0f, 40.0f), 2.1f, Random.Range (-40.0f, 40.0f));

		// instantiate
		GameObject GO = Instantiate (powerUpPrefabs [Random.Range (0, 2)], position, Quaternion.identity, powerUpParent);
		powerUps.Add(GO);
	}

	private void spawnPart()
	{	
		// spawn part at position
		GameObject part = Instantiate(partsPrefabs[partsSpawned], partsParent);
		part.transform.position = new Vector3(Random.Range (-40.0f, 40.0f), 0.5f, Random.Range (-40.0f, 40.0f));

		// add parts to parts list and increase partsSpawned
		parts.Add(part);
		partsSpawned++;

	}

	private void spawnAllPlayers()
	{
		// start a new list of players everytime
		players = new List<GameObject>();

		// for each player to spawn ..
		for ( int i = 0; i < playersToSpawn; i++ )
		{	
			// instantiate player
			GameObject GO = Instantiate(astronautPrefab);
			GO.transform.position = spawnPositions[i].position;
			players.Add(GO);

			// set player number
			int playerNumber = i + 1;

			// Set up player with playernumber, control and port
			GO.GetComponent<Player>().setup(playerNumber, controls[i], ports[i]);
		}
	}


	private void setCameraTargets ()
	{
		// set the cameran targets to be the players' transform
		Transform[] targets = new Transform[playersToSpawn];
		for (int i = 0; i < playersToSpawn; i++)
		{
			targets[i] = players[i].transform;
		}

		// These are the targets the camera should follow.
		m_CameraControl.m_Targets = targets;
	}

	// This is called from start and will run each phase of the game one after another.
	private IEnumerator gameLoop ()
	{

		// Start off by running the 'RoundStarting' coroutine but don't return until it's finished.
		yield return StartCoroutine (roundStarting ());

		// Once the 'RoundStarting' coroutine is finished, run the 'RoundPlaying' coroutine but don't return until it's finished.
		yield return StartCoroutine (roundPlaying ());

		Debug.Log("game not tied");
		roundEnding ();
		// EndGame();
	}

	private IEnumerator roundStarting ()
	{	
		// set roundRunning bool
		roundRunning = true;
		
		//Initializes the score to zero
		score = 0;
		updateScore(0); // set score to 0

		//Start PowerUp counter
		powerUpSpawntime = remainingTime - Random.Range (5f, 20f);
		asteroidSpawnTime = remainingTime - Random.Range(5f, 20f);
		partSpawnTime = remainingTime - Random.Range(0f, 10f);

		// Snap the camera's zoom and position to something appropriate for the reset tanks.
		m_CameraControl.SetStartPositionAndSize ();

		// Display text showing the players that the game has started.
		m_Setup.m_MessageText.text = "Game Start!";

		// Wait for the specified length of time until yielding control back to the game loop.
		yield return shortWait;
	}


	private IEnumerator roundPlaying ()
	{		
		// set can be paused to true
		canBePaused = true;

		// Clear the text from the screen.
		m_Setup.m_MessageText.text = string.Empty;

		// While there is time remaining
		while (remainingTime > 0f) {
			// ... return on the next frame.
			yield return null;
		}
	}


	private void roundEnding ()
	{
		// stop the roundrunning
		roundRunning = false;

		// stop the players from moving
		disablePlayerControls();

		// show the endgamebutton and dont allow pause
		m_Setup.m_EndGameButton.SetActive(true);
		canBePaused = false;

		// stop all other animations and stuff
		StopAllCoroutines();

		// if all parts are found ...
		if ( score == partsToSpawn) 
		{
			// get the time spent
			float startingTime = float.Parse(m_Setup.m_GameLength.text);
			float timeSpent = startingTime - remainingTime;

			// make the min string
			float minsLeft = Mathf.Floor(timeSpent/60);
			string minString = "";
			if ( minsLeft > 0 )
			{
				minString = minsLeft + " min ";
			}

			// make the sec string
			float secsLeft = Mathf.Round(timeSpent % 60);
			string secString = "";
			if ( secsLeft > 0 )
			{
				secString = secsLeft + "s";
			}

			// show the msg
			m_Setup.m_MessageText.text = "Mission accomplished in: \n"+ minString + secString+"!";

		// otherwise..
		} else 
		{
			// play the loser music
	        audioSource.loop = false;
			audioSource.clip = loseMusic;
			audioSource.Play();

			// show the loser words
			m_Setup.m_MessageText.text = "You lose!";
		}
	}

	private IEnumerator startGame ()
	{	
		//Close setup dialog
		m_Setup.gameObject.SetActive (false);

		//Show game objective
		m_Setup.m_MessageText.text = "Find the parts of your ship!";

		// start with score 0
		score = 0;

		// Wait for the specified length of time 
		yield return longWait;

		//Initialize game
		parts = new List<GameObject>();
		powerUps = new List<GameObject>();
		spawnAllPlayers ();
		setCameraTargets ();

		//Remove game title
		m_Setup.m_MessageText.text = string.Empty;

		//Start game loop
		StartCoroutine (gameLoop ());
	}

	private void disablePlayerControls()
	{
		foreach (GameObject player in players)
		{
			player.GetComponent<PlayerMovement>().disable();
		}
	}

	public void updateScore (int point)
	{
		// set top score to the score
		m_Setup.m_BlueScoreText.text = score+"/"+partsToSpawn;

		// point == 0 is at initial score showing
		if (point == 0) {
			// Enable GUI
			m_Setup.m_Image.SetActive (true);

			// show player names
			m_Setup.m_BlueTeamText.text = player1Name;
			m_Setup.m_RedTeamText.text = player2Name;

		// point == 1 is to add score by 1
		} else if (point == 1)
		{	
			// if all parts are found
			if ( score == partsToSpawn)
			{
				roundEnding();
			}
		}
	}

	public void toggleExtraSettings ()
	{
		// If 2v2 has not been selected
		if (m_Setup.m_Gamemode.value == 0) {
			// Disable extra settings
			m_Setup.m_ExtraSettings.SetActive (false);
		} else {
			// Enable Extra settings
			m_Setup.m_ExtraSettings.SetActive (true);
		}
	}

	// Shows the inputfield for entering COM number
	public void toggleSerial ()
	{
		if (m_Setup.m_BlueControl.value == 1) {
			m_Setup.m_BluePort.gameObject.SetActive (true);
		} else {
			m_Setup.m_BluePort.gameObject.SetActive (false);
		}
		if (m_Setup.m_RedControl.value == 1) {
			m_Setup.m_RedPort.gameObject.SetActive (true);
		} else {
			m_Setup.m_RedPort.gameObject.SetActive (false);
		}
	}

	private IEnumerator DisplayText (string text)
	{
		// Show text
		m_Setup.m_MessageText.text = text;

		// Wait for the specified length of time 
		yield return shortWait;

		// Remove text
		m_Setup.m_MessageText.text = string.Empty;
        
	}

	private string GetPortName (string port)
	{
		if ( port == "")
		{
			return "";
		}
		// If running on windows
		if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer) {

			if (int.Parse (port) > 9) {
				return "\\\\.\\COM" + port;
			} else {
				return "COM" + port;

			}
		} else {
			return "/dev/cu.usbmodem" + port;

		}
	}

	// Pausing game by setting timescale to 0. Better ways to do this but this is easy and was in a hurry.
	public void pauseGame()
	{
		isPaused = !isPaused;
		if(Time.timeScale == 1)
		{
			pauseScreen.SetActive(true);
			Time.timeScale = 0;
		}
		else
		{
			pauseScreen.SetActive(false);
			Time.timeScale = 1;
		}
	}
	
	// Ending game manually
	public void endGame()
	{
		// stop roundRunning
		roundRunning = false;

		// cannot be paused
		canBePaused = false;

		// leave screen
		m_Setup.m_EndGameButton.SetActive(false);
		m_Setup.m_Image.SetActive (false);
		pauseScreen.SetActive(false);

		// end all coroutines
		StopAllCoroutines();

		// move camera
		m_CameraControl.m_Targets = cameraTargetTemp;

		// allow time to flow
		Time.timeScale = 1;

		// reset everything
		resetGame();

		// show setup menu
		m_Setup.gameObject.SetActive (true);

		// play menu music
        audioSource.loop = true;
        audioSource.clip = menuMusic;
        audioSource.Play();

		remainingTime = 0f;
		m_Setup.m_CounterText.text = "";
		m_Setup.m_BlueScoreText.text = "";
		m_Setup.m_BlueTeamText.text = "";

	}

	private void resetGame()
	{
		// destroy everything to what it was
		destroyAllPlayers();
		destroyAllParts();
		destroyAllPowerUps();
		resetTiles();

		// reset everything to what it was
		partsSpawned = 0;
		isPaused = false;
	}

	private void resetTiles()
	{
		foreach (Transform child in TileParent)
		{
			child.GetComponent<Tile>().revealed = false;
		}
	}

	private void destroyAllPlayers()
	{
		foreach ( GameObject player in players)
		{
			Destroy(player);
		}
	}

	private void destroyAllParts()
	{
		foreach (GameObject part in parts)
		{
			Destroy(part);
		}
	}

	private void destroyAllPowerUps()
	{
		foreach (GameObject powerUp in powerUps)
		{
			Destroy(powerUp);
		}
	}

	// last 20 secs protocol
	private void activateLast20secsProtocol()
	{		
		last20secActivated = true;

		// make all the part surface from the sand
		if ( parts.Count != 0)
		{
			foreach (GameObject part in parts)
			{
				part.gameObject.GetComponent<hidding>().activateLast20Secs();
			}	
		}
		
	}

	public void exit()
	{
		Debug.Log("exit");
		Application.Quit();
	}
}