using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hidding : MonoBehaviour
{   
    // store the hidden state
    private bool hidden; 

    // store the normal and hidden material
    public Material normalMaterial;

    // store sand material
    public Material yellowLight;

    // store the object's renderer
    private Renderer renderer;

    // store this var for special updates at the final 20s
    private bool last20secs = false;

    private void Start()
    {
    	hide();    	

        // if object is a power up
    	if (this.gameObject.layer == 12 )
		{
            // find the renderer in children objects
	        renderer = this.GetComponentInChildren<Renderer>();

        // if object is parts
		} else if ( this.gameObject.layer == 15)
		{
            // find the renderer at same level
			renderer = this.gameObject.GetComponent<Renderer>();
		}
    }
    

    private void Update()
    {
        // if last 20 secs and for parts
        if ( last20secs && this.gameObject.layer == 15 ) 
        {
            // if hidden ..
            if ( hidden )
            {
                // change material to sand material
                renderer.material = yellowLight;
                renderer.enabled = true;
            }
                
            // if not hidden
            if ( !hidden )
            {       
                // set material to normal
                renderer.material = normalMaterial;
            }
        }

        // for both parts and power up
        // if hidden ..
    	if ( hidden )
    	{     
            // hide unless is last 20 seconds
            if ( !last20secs )
            {
                // disappear
                renderer.enabled = false;
            }
               
        // show as long as not hidden no matter how much time is left
    	} else {
            // appear
    		renderer.enabled = true;
    	}
    }

    public void unhide()
    {
        hidden = false;
    }
		
    public void hide()
    {
        hidden = true;
    }

    public void activateLast20Secs()
    {
        last20secs = true;
    }
}