﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{	
	// store player animator and controlelr
	private Animator anim;
	private CharacterController controller;

	// store the default speeds and the speed var
	public float normalSpeed = 75.0f;
	public float fastSpeed = 125f;
	private float speed;

	// store turnspeed
	public float turnSpeed = 250.0f;

	// gravity
	public float gravity = 20.0f;

	// store variables that power up may affect
	private bool invulnerable;
	[HideInInspector]public bool movementEnabled { get; private set; }

	// store duration of power up
	public float durationOfPowerUp = 8f;

	// store movement variables
	public float updown;
	public float leftright;

    // Start is called before the first frame update
    void Start()
    {
    	// get controller and animator
        controller = GetComponent <CharacterController>();
		anim = gameObject.GetComponentInChildren<Animator>();

		// set starting values
		movementEnabled = true;
		invulnerable = false;
		speed = normalSpeed;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
    	// if not stunned ..
    	if (movementEnabled)
    	{
			// if movement detected ..
			if ( updown != 0 || leftright != 0 ) {
				// set state "AnimationPar" to 1
				anim.SetInteger ("AnimationPar", 1);
			}  else {
				// set state "AnimationPar" to 0
				anim.SetInteger ("AnimationPar", 0);
			}


			// update movement variables
			Vector3 xAxisMovement = new Vector3(0f, 0f, 0f);
			Vector3 zAxisMovement = new Vector3(0f, 0f, 0f);

			//use updown and leftright to get axis movement
			if ( updown > 0 )
			{
				xAxisMovement = Vector3.forward;
				
			} else if ( updown < 0 )
			{
				xAxisMovement = Vector3.back;
			} else {
				xAxisMovement = Vector3.zero;
			}

			if ( leftright > 0)
			{
				zAxisMovement = Vector3.right;
			} else if ( leftright < 0 )
			{
				zAxisMovement = Vector3.left;
			} else {
				zAxisMovement = Vector3.zero;
			}
	        Vector3 FinalAxisMovement = (xAxisMovement + zAxisMovement) * 0.5f;

	        //Rotate towards where character needs to move
	        if (xAxisMovement != Vector3.zero || zAxisMovement != Vector3.zero)
	        {
	            Quaternion rotateTowards = Quaternion.LookRotation(FinalAxisMovement, Vector3.up);
	            transform.rotation = rotateTowards;
	        }

	        // include gravity in movement
	        Vector3 Movement = Physics.gravity;
	        if ( updown != 0 || leftright != 0)
	        {
	            Movement += transform.forward;
	        }

	        // move
	        Vector3 NormalizedMovement = Movement.normalized * Time.deltaTime * speed;
	        controller.Move(NormalizedMovement);

	    // if stunned
    	} else {
    		// stop walking animation
			anim.SetInteger ("AnimationPar", 0);
    	}
    }

    public void stun()
    {
    	if (!invulnerable)
    	{
	    	StartCoroutine(stunRoutine());
    	}
    }

    private IEnumerator stunRoutine()
    {
    	this.movementEnabled = false;
    	yield return new WaitForSecondsRealtime ( 3 );
    	this.movementEnabled = true;
    }

    public void addSpeed()
    {
    	StartCoroutine(addSpeedRoutine());
    }

    private IEnumerator addSpeedRoutine()
    {
    	this.speed = fastSpeed;
    	yield return new WaitForSecondsRealtime(durationOfPowerUp);
    	this.speed = normalSpeed;
    }

    public void becomeInvulnerable()
    {
    	StartCoroutine(becomeInvulnerableRoutine());
    }

    private IEnumerator becomeInvulnerableRoutine()
    {
    	this.invulnerable = true;
    	yield return new WaitForSecondsRealtime(durationOfPowerUp);
    	this.invulnerable = false;
    }

    public void disable()
    {
    	this.movementEnabled = false;
    }
}



