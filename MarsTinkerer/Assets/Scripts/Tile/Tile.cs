﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Tile : MonoBehaviour
{   
    // store the materials
	public Material Yellow;
	public Material DarkYellow;

    // store the tile's meshrenderer
    private MeshRenderer MeshRend;
    
    // neccessary var
    public bool revealed;
    private bool inContact;
	private float remainingTime; // remaining time before tile covers up (unreveal)
    public float durationOfReveal = 4f;

    // store the item assigned to this tile
    private GameObject item;

    // Start is called before the first frame update
    void Start()
    {   
        // starting vars
        MeshRend = GetComponent<MeshRenderer>();
        remainingTime = -1f;
        revealed = false;
        inContact = false;
    }

    // Update is called once per frame
    void Update()
    {
        // if remaining time is 
        // if ( remainingTime > 0f)
        // {
        //     remainingTime -= Time.deltaTime;
        // }

        // if tile is revealded and not in contact
        if ( revealed && !inContact )
        {
            if ( remainingTime > 0f)
            {
                // start count down
                remainingTime -= Time.deltaTime;
            } else {
                // after counting, unreveal
                revealed = false;
            }
        }

        // if revealed
        if ( revealed )
        {   
            // darkyellow
            MeshRend.material = DarkYellow;
        } else { 
            // yellow
            MeshRend.material = Yellow;
        }

        // For hidding and unhidding of items
        if ( this.item != null ) 
        {
            if ( revealed )
            {
                // unhide object
                this.item.gameObject.GetComponent<hidding>().unhide();
            } else {
                // hide object
                this.item.gameObject.GetComponent<hidding>().hide();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // if a player collides
    	if (other.gameObject.layer == 9) 
        {
	        this.inContact = true;
            this.revealed = true;
    	}

        // if part or power up collides 
        if ( (other.gameObject.layer == 12||other.gameObject.layer == 15) && this.item == null )
        {
            // Debug.Log(other.gameObject.name);
            assignItemToTile(other.gameObject);
        }
    }


    private void OnTriggerExit(Collider other)
    {
        // if player exits the tile
        if ( other.gameObject.layer == 9 ) // player layer
        {   
            // start counter
            remainingTime = durationOfReveal;
            this.inContact = false;
        }
    }

    public bool assignItemToTile(GameObject item)
    {
        if ( this.item == null)
        {   
            // Debug.Log("assigning tile");
            this.item = item;
            return true;
        } else {
            // Debug.Log("tile has already been assigned");
            return false;
        }
    }
}
