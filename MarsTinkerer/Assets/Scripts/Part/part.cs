﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class part : MonoBehaviour
{	
	// store the gamemanager
	public GameManager gameManager;

	// store the audio source and the music 
	public AudioSource audioSource;
	public AudioClip partCollectedMusic;
	
	private bool triggered = false;

	void Start()
	{	
		// set game manager
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager>();

		// get the audio source ready to play the music
		audioSource.clip = partCollectedMusic;
	}

	void OnTriggerEnter(Collider other)
	{
		// if player is hit
		if ( other.gameObject.layer == 9 && other.GetType() == typeof(UnityEngine.CharacterController) ) // layer 8 is player
		{	
			if ( !triggered)
			{
				// set triggered to true so that repeated collision wont count
				triggered = true;

				// turn off the renderer and hidding script so the part will disappear
				this.gameObject.GetComponent<hidding>().enabled = false;
				this.gameObject.GetComponent<Renderer>().enabled = false;

				// play the music
				audioSource.Play();

				// destory the part after music ends
				Destroy(gameObject, partCollectedMusic.length);

				// update the score
				gameManager.GetComponent<GameManager>().score += 1;
				gameManager.GetComponent<GameManager>().updateScore(1);
			}
			
		}
	}
}
